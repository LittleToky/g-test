extends KinematicBody2D

const vel_length = 1.0;
const distance = 100.0;
var center_pos;
var vel;

func _ready():
	center_pos = Vector2(0, 0);
	position = center_pos + Vector2(0, - distance);
	vel = Vector2(vel_length, 0);
	
func _physics_process(delta):
	var to_center = (center_pos - position);
	var a_length = pow(vel_length, 2) / distance;
	vel += a_length * transform.xform(to_center).normalized();
	
	move_and_collide(vel);
